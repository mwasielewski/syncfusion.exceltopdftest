﻿using Syncfusion.Pdf;
using Syncfusion.XlsIO;
using Syncfusion.XlsIORenderer;
using System;
using System.IO;

namespace ExcelToPdfTest
{
    class Program
    {
        static void Main(string[] args)
        {
            using (ExcelEngine excelEngine = new ExcelEngine())
            {
                System.Globalization.CultureInfo.CurrentCulture = new System.Globalization.CultureInfo("pl-PL");
                IApplication application = excelEngine.Excel;

                //Initialize XlsIO renderer.
                XlsIORenderer renderer = new XlsIORenderer();

                var excelStream = new FileStream("Resources/ExcelTestFile.xlsx", FileMode.Open, FileAccess.Read);
                IWorkbook workbook = application.Workbooks.Open(excelStream);

                //Convert Excel document with charts into PDF document 
                PdfDocument pdfDocument = renderer.ConvertToPDF(workbook);

                Stream stream = new FileStream("Resources/SyncfusionConverterdFile.pdf", FileMode.Create, FileAccess.ReadWrite);
                pdfDocument.Save(stream);

                excelStream.Dispose();
                stream.Dispose();
            }
        
            
        }
    

            
    }
}
